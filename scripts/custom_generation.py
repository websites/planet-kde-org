# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
# SPDX-FileCopyrightText: 2021-2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.0-or-later

import concurrent.futures
import configparser
import copy
import logging
import os
import re
import shutil
import socket
import time
import unicodedata
from argparse import Namespace
from importlib.metadata import version

import feedparser
import yaml
from hugo_gettext import utils
from hugo_gettext.compilation import compile_po
from hugo_gettext.config import initialize
from hugo_gettext.generation.g_domain import HugoDomainG
from hugo_gettext.generation.g_lang import HugoLangG
from hugo_gettext.generation.index import Generation
from hugo_gettext.generation.renderer_hugo_l10n import RendererHugoL10N
from hugoi18n import customs
from hugoi18n.fetch import revert_lang_code
from markdown_gettext.domain_generation import gettext_func


def parameterize(string, sep='-'):
    """
    Mimic Ruby String#parameterize function
    :param string:
    :param sep:
    :return:
    """
    parameterized = unicodedata.normalize('NFKD', string).encode('ASCII', 'ignore').decode()
    parameterized = re.sub(r'[^a-zA-Z\d\-_]+', sep, parameterized)
    parameterized = re.sub(rf'{sep}{{2,}}', sep, parameterized)
    return parameterized.strip(sep).lower()


def render_post(file_path, feed_fields, site_link, entry, index):
    # only forward to Discuss the first entry
    if index != 0:
        feed_fields['discuss'] = False
    published = entry.published_parsed if 'published_parsed' in entry else entry.updated_parsed
    published_date = time.strftime('%Y-%m-%d', published)
    file_name = f"{parameterize(feed_fields['author'])}-{published_date}-{parameterize(entry.title)}"
    content = entry.content[0]['value'] if 'content' in entry else entry.summary if 'summary' in entry else ''

    # Remove any CDATA markup, we handle that in the template
    content = re.sub(r'^<!\[CDATA\[(.+)\]\]>$', r'\g<1>', content)
    # Make absolute links with site url
    cut_url = re.search(r'//.*?(?=/|$)', site_link).group(0) + '/'
    content = re.sub(r'(?<=src=[\"\'])/(?!/)', cut_url, content)
    content = re.sub(r'(?<=href=[\"\'])/(?!/)', cut_url, content)
    # Make browsers not report page as having non-HTTPS elements
    content = re.sub(r'(?<=src=[\"\'])http:', 'https:', content)
    content = re.sub(r'(?<=href=[\"\'])http:', 'https:', content)
    # Open links in new tabs
    content = re.sub(r'(href=[\"\']https://)', r'target="_blank" \1', content)
    # Make page style consistent
    content = re.sub(r'<link [^>]*rel=[\"\']stylesheet[^>]+>', '', content)
    content = re.sub(r'<style[^>]*>.*?</style>', '', content, flags=re.M | re.S)
    # Replace iframe elements with a message telling users to look for them in original posts
    content = re.sub(r'<iframe[^>]*/>', '<i>An iframe has been removed to ensure Planet works properly. '
                                        'Please find it in the original post.</i>', content, flags=re.M)
    # Escaping braces make scripts inside content invalid. Let's remove scripts altogether
    content = re.sub(r'<script[^>]*>.*?</script>',
                     '<i>A script element has been removed to ensure Planet works properly. '
                     'Please find it in the original post.</i>', content, flags=re.M | re.S)  # dot matches all
    # After including date in each post file name, some posts appear with shortcode snippets in the content
    content = re.sub('{', '&#123;', content)
    # Site specifics
    # https://outsideofinfinity.wordpress.com/2020/05/04/what-is-krita-up-to-now/
    content = re.sub(r'(id[:=] ?)[\'"]atatags-[^"\']*[\'"]', r'\1""', content)
    # https://labplot.kde.org/2022/03/22/labplot-2-9-beta/ and others on https://labplot.kde.org
    content = re.sub(r'data-rel="lightbox-gallery-[^"]*"', '', content)
    # Embed videos from KDE PeerTube
    if feed_fields['feed_url'].startswith('https://tube.kockatoo.org/'):
        embed_link = f'https://tube.kockatoo.org/videos/embed/{entry.link.split("/")[-1]}'
        content += f'\n<iframe src="{embed_link}" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>'

    post_fields = feed_fields.copy()
    post_fields['title'] = entry.title
    post_fields['date'] = time.strftime('%Y-%m-%dT%H:%M:%S%z', published)
    post_fields['lastmod'] = time.strftime('%Y-%m-%dT%H:%M:%S%z', entry.updated_parsed)
    post_fields['post_url'] = entry.link
    post_fields['guid'] = entry.id
    return {'path': f'{file_path}/{file_name}.html',
            'updated': entry.updated_parsed,
            'fm': post_fields,
            'content': content}


def process_feed(feed_id, feed_config) -> list:
    if 'feed_url' not in feed_config:
        logging.warning(f'{feed_id} has no feed_url')
        return []
    logging.info(f'processing feed {feed_id}')
    feed_url = feed_config['feed_url']
    # all HTTP libraries work with sockets, so we set timeout for each socket operation
    socket.setdefaulttimeout(120)

    feed_data = feedparser.parse(feed_url, resolve_relative_uris=False, sanitize_html=False)
    if feed_data.bozo:
        exception = feed_data.bozo_exception
        if type(exception) not in {feedparser.exceptions.CharacterEncodingOverride}:
            logging.warning(f'feed {feed_id}: {type(exception)}, {exception}')
            return []
    site_link = feed_data.feed.get('link', '/'.join(feed_url.split('/')[:3]))
    site_url = feed_config.get('site_url', site_link)
    feed_fields = {'feed_url': feed_url,
                   'site_url': site_url,
                   'avatar': feed_config.get('avatar', ''),
                   'discuss': int(feed_config.get('discuss', 0)) != 0}
    for flair in feed_config.get('flairs', '').split():
        if ':' in flair:
            parts = flair.split(':', 1)
            feed_fields[parts[0]] = parts[1]
        else:
            feed_fields[flair] = True
    feed_title = feed_config.get('title', '')
    in_feed_title = feed_data.feed.get('title', '')
    logging.info(f'title in config: {feed_title}; title in feed: {in_feed_title}')
    feed_fields['author'] = feed_title or in_feed_title
    feed_fields['feed_summary'] = feed_data.feed.get('subtitle', '')
    feed_fields['format'] = feed_data.version
    feed_lang = feed_config.get('lang', 'en')
    file_path = 'content' if feed_lang == 'en' else f'content-trans/{feed_lang}'
    os.makedirs(file_path, exist_ok=True)
    return [render_post(file_path, feed_fields, site_link, entry, index)
            for index, entry in enumerate(feed_data.entries)]


def generate_posts():
    config = configparser.RawConfigParser()
    config.read('planet.ini')
    feed_ids = list(config.keys())
    try:
        feed_ids.remove('DEFAULT')
    except KeyError:
        pass
    entries = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
        # Mark each future with its feed id
        future_to_feed = {executor.submit(process_feed, feed_id, config[feed_id]): feed_id for feed_id in feed_ids}
        for future in concurrent.futures.as_completed(future_to_feed):
            feed_id = future_to_feed[future]
            try:
                feed_entries = future.result()
                entries.extend(feed_entries)
            except Exception as e:
                logging.warning('Feed %r generated an exception (%s): %s' % (feed_id, type(e), e))
            else:
                logging.info(f'Processed feed {feed_id}')
    entries.sort(key=lambda x: x['updated'], reverse=True)
    for e in entries[:1300]:
        logging.info(f"{e['path']}: {time.strftime('%Y-%m-%dT%H:%M:%S%z', e['updated'])}")
        with open(e['path'], 'w') as post_file:
            post_file.write('---\n')
            post_file.write(yaml.dump(e['fm'], default_flow_style=False, allow_unicode=True))
            post_file.write('---\n')
            post_file.write(e['content'])


domain_name = "planet-kde-org"


def generate():
    generate_posts()

    if not (os.path.isdir('content') and len(os.listdir('content')) > 0):
        return

    os.environ["PACKAGE"] = 'websites-planet-kde-org'
    hg_config, mdi = initialize(RendererHugoL10N, customs.__file__)
    configs = hg_config.hugo_config
    en_strings = utils.read_file(hg_config.string_file_path)
    original_configs = copy.deepcopy(configs)
    g = Generation(en_strings, {}, hg_config, mdi)

    content_lang_codes = ['en']
    for lang in os.listdir('content-trans'):
        if len(os.listdir(f'content-trans/{lang}')) > 0:
            content_lang_codes.append(lang)
    lang_config = list(configs['languages'].keys())
    for config_lang_code in lang_config:
        if config_lang_code not in content_lang_codes:
            del configs['languages'][config_lang_code]

    po_dir = 'po'
    compile_po(Namespace(dir=po_dir))

    for hugo_lang_code in content_lang_codes:
        if hugo_lang_code in ['en']:
            continue
        else:
            lang_code = revert_lang_code(hugo_lang_code)
            os.environ["LANGUAGE"] = lang_code
            tr = gettext_func(domain_name)

            lang_g = HugoLangG(g, lang_code)
            lang_g.default_domain_g = HugoDomainG(lang_g, tr)
            strings_result = lang_g.localize_strings()
            i18n_strings = strings_result.localized
            lang_g.write_strings(i18n_strings)
            lang_g.localize_languages()
            configs['languages'][hugo_lang_code]['contentDir'] = f'content-trans/{hugo_lang_code}'
            lang_g.localize_menu()
            lang_g.localize_description()
            lang_g.localize_title()

    shutil.rmtree('locale')
    if configs != original_configs:
        utils.write_file(hg_config.config_path, configs)


if __name__ == "__main__":
    level = logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=level)
    hugoi18n_version = version('hugoi18n')
    logging.info(f'Using hugoi18n v{hugoi18n_version}')
    generate()
