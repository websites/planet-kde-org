#
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: websites-planet-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2022-02-07 09:12+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr "كوكب كِيدِي"

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr "موقع كِيدِي يقدم أحدث الأخبار عن مشروع كِيدِي"

#: config.yaml:0
msgid "Add your own feed"
msgstr "أضف تلقيمك"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr "مرحبا في كوكب كِيدِي"

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""
"هذا مجمع تلقيمات يجمع ما يكتبه المساهمون في [مجتمع كِيدِي](https://kde.org) في "
"مدوناتهم الخاصة بلغات مختلفة"

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr "اذهب تدوينة واحدة للأسفل"

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr "اذهب تدوينة واحدة للأعلى"
