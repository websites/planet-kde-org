#
# Kheyyam Gojayev <xxmn77@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: websites-planet-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2021-05-28 22:19+0400\n"
"Last-Translator: Kheyyam Gojayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.04.1\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr "KDE planeti"

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr "KDE planeti saytı KDE layihəsindən ən yenio xəbərləri çatdırır"

#: config.yaml:0
msgid "Add your own feed"
msgstr "Öz xəbər lentinizi əlavə edin"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr "KDE Planetinə Xoş Gəldiniz"

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""
"Bu, [KDE icmasına] (https://kde.org) töhvə verən iştirakçıların öz "
"bloqlarında fərqli dillərdə yazdıqlarını xəbər lenti olaraq bir yerdə "
"cəmləyir"

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr "Bir məqalə aşağıya keçin"

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr "Bir məqalə yuxarıya keçin"
